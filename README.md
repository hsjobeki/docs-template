# docs-template

## Template with Gitlab Pages - CI/CD preconfigured

This page is build using Gitlab CI/CD

* The rendered page is available under [https://hsjobeki.gitlab.io/docs-template/](https://hsjobeki.gitlab.io/docs-template/)
* Your documentation should also appear at {__username__}.gitlab.io/{__repository__}

Have a look at the __.gitlab-ci.yaml__ if you are interested how easy it works!

Note: Your Gitlab must have Gitlab-Pages enabled. If enabled you can also view your active pages under: __Settings > Pages__

## Getting started

For full mkdocs documentation visit [mkdocs.org](https://www.mkdocs.org)

Be Aware: __By convention we dont use any plugins__!

Only the Draw.io (Diagrams.net) plugin for some visual diagrams
can be found  [here](https://github.com/LukeCarrier/mkdocs-drawio-exporter)

Keep your docs minimal, clean and comprehensive.

## Common Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
